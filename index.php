<?php

require_once __DIR__ . '/Base.php';
$result =  new Base();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test Task</title>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<body>

<table>
    <tr>
        <th>Symbol</th>
        <th>Name</th>
        <th>Price</th>
        <th>Unique Identifier</th>
    </tr>
    <?php
    if($result->content):
    foreach ($result->content as $item): ?>
    <tr>
        <td><?=$item['symbol']?></td>
        <td><?=$item['name']?></td>
        <td><?=$item['price']?></td>
        <td><?=$item['apiId']?></td>
    </tr>
    <?php  endforeach; else: ?>
    <tr>
        <td colspan="4">Sorry, results not found</td>
    </tr>
    <?php endif; ?>
</table>
</body>
</html>
