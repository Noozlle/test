<?php


class DB
{
    protected $config;
    protected $connection;
    protected function __construct()
    {
        $this->config = require_once __DIR__ . '/config.php';
        $this->connection = new PDO($this->config['dsn'], $this->config['username'], $this->config['password']);
        $testConnection =  $this->testConnection();
       if(!$testConnection || $testConnection == null){
          $this->createTables();
       }
    }
    protected function testConnection(){
        try {
            $result = $this->checkIfTableExists($this->connection);
            return $result;
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    protected function checkIfTableExists($connection){
        try {
            $result = $connection->prepare("SELECT * FROM coins LIMIT 1");
            $result->execute();
            $row = $result->fetch();
        } catch (Exception $e) {
            return FALSE;
        }
        return $row;
    }

    protected function createTables(){
        try {
            $sql ="CREATE TABLE IF NOT EXISTS coins (
                    id int unsigned primary key NOT NULL AUTO_INCREMENT,
                    apiId varchar(50) NOT NULL,
                    symbol varchar(5) NOT NULL,
                    name varchar(50) NOT NULL,
                    price float
                    );" ;
            $this->connection->exec($sql);
            $this->getApiData();
           return true;
        } catch(PDOException $e) {
            return  false;
        }
    }


    protected function fillTable($data){
        $statement = $this->connection->prepare('INSERT INTO coins (apiId, symbol, name, price)
         VALUES (:apiId, :symbol, :name, :price)');
        foreach ($data as $row){
            $statement->execute($row);
        }

    }

    protected function getApiData(){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->config['apiURL']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 3);
        $data = trim(curl_exec($curl));
        curl_close($curl);
        $arr = json_decode($data, TRUE);
        $slicedArr = array_slice($arr,0,50);
        $result = $this->filterArray($slicedArr);
        $this->fillTable($result);
    }

    private function filterArray($array){
        $newArray = [];
        for($i = 0; $i < count($array); $i++){
            $tmp = [
                'apiId'=>$array[$i]['id'],
                'symbol'=>$array[$i]['symbol'],
                'name'=>$array[$i]['name'],
                'price'=>$array[$i]['current_price']
            ];
            array_push($newArray , $tmp);
        }
        return $newArray;
    }

}